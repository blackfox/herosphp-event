# herosphp/event

增加事件机制，主要是为了进行代码解耦。

## install
```shell
    composer install herosphp/event
```


### publish config file
```shell
    composer vendor:publish "herosphp/event"
```


### init class
```php
<?php
declare(strict_types=1);
namespace app\init;

use herosphp\core\Config;
use herosphp\utils\Logger;
use herosphp\plugin\event\EventStarter as BaseEventStarter

class EventStarter extends BaseEventStarter
{
    protected static bool $debug = false;
}

EventStarter::init();

```
