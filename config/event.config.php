<?php
declare(strict_types=1);

return [
    //key=> callback array
    'demo.event' => [
        function (string $userId) {
            echo "config.user.login event,userId:{$userId}".PHP_EOL;
        },
    // [UserLoginEvent::class,'demo'],
    // [UserLoginEvent::class,'demo2'], //static method
    ],
];
